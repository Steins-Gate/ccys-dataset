# CCYS Dataset

## Overall Information  

The data has been pre-processed and segmented for training and test purposes.
The data are stored in `.pickle` format.
For example,
one can use 
```
pickle.load(open(data_path + "CCYS_train_data.p", 'rb'))
```
to load the training data.  

The sizes of the data are:  

* CCYS_train_data: 53591 X 357;  

* CCYS_train_labels: 53591 X 75;  

* CCYS_test_data: 26397 X 357;  

* CCYS_test_labels: 26397 X 75.  

## Description  

We collected the real dataset during the research project called the Communities that Care Youth Survey (CCYS), which is administered and funded by the Louisiana government.
The data are collected from 79,988 students from the 6th-, 8th-, 10th-, and 12th-grades enrolled in public (some private) schools throughout the State of Louisiana.
In total 355 features are designed and collected through questionnaires provided by 454 independent agencies.
These questionnaires assess the students’ exposure to a set of risk and protective factors (*e.g.* family, neighborhood, school, *e.t.c.*) which have been validated to impact the students' social behaviors.
For example, students who live in disorganized, crime-ridden neighborhoods are more likely to become involved in crime and drug abuse than that who live in safe ones.
As the students are not obligated to answer all the questionnaires, and in practice, they are more likely to only answer the questions that they are interested, the feature sets describing different students are non-identical.
Our goal is to predict the students's involvement in 75 different behaviors, including those pertaining to academic performance (*e.g.* attendance or dropout), 
anti-social behavior (*e.g.* bullying or delinquency),
substance abuse (*e.g.* alcohol or drug),
and mental health (*e.g.* depression or suicide-inclination).

## Citation 

When using this dataset, please cite our below research papers:

@article{Biggar2016JRSS,
  title={Criminal futures on the ruralside: A preliminary examination of antisocial behaviors of rural and urban students},
  author={Biggar Jr, Raymond and Chen, Jing and Forsyth, Craig J},
  journal={Journal of Rural Social Sciences},
  volume={31},
  number={2},
  pages={16},
  year={2016},
  publisher={Southern Rural Sociology Association}
}

@article{Biggar2016DB,
  title={Protective factors for deviance: A comparison of rural and urban youth},
  author={Biggar Jr, Raymond W and Forsyth, Craig J and Chen, Jing and Richard, Tayler A},
  journal={Deviant Behavior},
  volume={37},
  number={12},
  pages={1380--1391},
  year={2016},
  publisher={Taylor \& Francis}
}